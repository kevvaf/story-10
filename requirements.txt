asgiref==3.2.7
certifi==2020.4.5.1
chardet==3.0.4
coverage==5.0.4
dj-database-url==0.5.0
Django==3.0.5
django-heroku==0.3.1
Faker==4.0.2
get==2019.4.13
gunicorn==20.0.4
idna==2.9
lxml==4.5.0
post==2019.4.13
psycopg2==2.8.4
public==2019.4.13
python-dateutil==2.8.1
pytz==2019.3
query-string==2019.4.13
request==2019.4.13
requests==2.23.0
selenium==3.141.0
six==1.14.0
sqlparse==0.3.1
text-unidecode==1.3
urllib3==1.25.8
whitenoise==5.0.1
