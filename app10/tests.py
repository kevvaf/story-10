from selenium import webdriver
from django.test import TestCase
from django.test import LiveServerTestCase
from selenium.webdriver.chrome.options import Options
from django.test import TestCase, Client
from django.urls import resolve, reverse
import time
from .views import Index

# Create your tests here.

class Story10Test(TestCase):
    def test_story10_url_is_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code,200)
        self.assertTemplateUsed(response, 'home.html')

    def test_story10_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, Index)

class Stroy8FuncTest(LiveServerTestCase):
    def setUp(self):
        super().setUp()
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        self.browser.maximize_window() #For maximizing window
        self.browser.implicitly_wait(20)
    def tearDown(self):
        self.browser.quit()
        super().tearDown()

    def test_landing_page(self):
        self.browser.get(self.live_server_url + '/')
        self.assertIn('story10', self.browser.title)
        time.sleep(3)

    def test_register(self):
        self.browser.get(self.live_server_url + '/')
        # regis=self.browser.find_element_by_css_selector('.reg')
        # regis.click()
        # first = self.browser.find_element_by_css_selector('#id_first_name')
        # last = self.browser.find_element_by_css_selector('#id_last_name')
        # email = self.browser.find_element_by_css_selector('#id_email')
        # uname = self.browser.find_element_by_css_selector('#id_username')
        # pass1 = self.browser.find_element_by_css_selector('#id_password1')
        # pass2 = self.browser.find_element_by_css_selector('#id_password2')
        test ='test'
        test_email = 'test@gmail.com'
        test_pass = 'story1010'
        # first.click()
        # first.send_keys(test)
        # last.click()
        # last.send_keys(test)
        # email.click()
        # uname.click()
        # uname.send_keys(test)
        # email.send_keys(test_email)
        # pass1.click()
        # pass1.send_keys(test_pass)
        # pass2.click()
        # pass2.send_keys(test_pass)
        # # button = self.browser.find_element_by_css_selector('button')
        # button.click()
        time.sleep(5)

        alert = 'story10'

        self.assertIn(alert, self.browser.page_source + 'login/')


